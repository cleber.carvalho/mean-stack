var express = require("express");
var consign = require("consign");
var bodyParser = require("body-parser");

// conexão com a base de dados
require('./database')('localhost/alurapic');

var app = express();

app.set("secret", "appteste");
app.use(express.static('./public'));
app.use(bodyParser.json());


consign({cwd : "app"})
        .include('models')
        .then('api')
        .then('routes/auth.js') //estamos garantindo que routes/auth sera carregado primeiro
        .then('routes')
        .into(app);


module.exports = app;