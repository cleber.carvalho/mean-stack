module.exports = function(uri){
    var mongoose = require('mongoose');
    
    mongoose.connect("mongodb://" + uri);

    mongoose.connection.on('connected', function(){
        console.log("Conectado ao mongoDB");
    });

    mongoose.connection.on("error", function(error){
        console.log("Erro na conexao com o banco de dados: " + error);
    });

    mongoose.connection.on("disconnected", function(){
        console.log("Banco de dados desconectado.");
    });

    process.on('SIGINT', function(){
        mongoose.connection.close(function(){
            console.log("Aplicação terminada, conexão encerrada.");
            process.exit(0);
        });
    });
};
