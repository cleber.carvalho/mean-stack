var mongoose = require("mongoose");

module.exports = function(app){
    var api = {};
    
    // solicitando o modelo 'Foto'
    var model = mongoose.model('Foto');

    api.lista = function(req, res){
        console.log("------------- chegou no controller next funcionou  -----------------");
        console.log(req.headers);
        model.find()
            .then(function(fotos){
                console.log("------------- enviando resposta  -----------------");
                res.json(fotos);
                console.log(res.json(fotos));
                console.log("------------- resposta enviada -----------------");
            }, function(error){
                console.log(error);
                res.sendStatus(500);
            });
    };
    
    api.buscaPorId = function(req, res){

        var id = req.params.id;

        model.findById(id)
            .then(function(foto){
                if (!foto) throw new Error('Foto não encontrada');
                res.json(foto);
            }, function(error){
                console.log(error);
                res.sendStatus(500);
            });
    };
    
    api.removePorId = function(req, res){
        
        var id = req.params.id;

        model.remove({_id : id})
            .then(function(foto){
                res.sendStatus(204);
            }, function(error){
                console.log(error);
                res.sendStatus(404);
            });
    };
    
    api.adiciona = function(req, res){
        var foto = req.body;

        model.create(req.body)
            .then(function(foto){
                res.json(foto);
            }, function(error){
                console.log(error);
                res.sendStatus(500);
            });
    }
    
    api.altera = function(req, res){
      model.findByIdAndUpdate(req.params.id, req.body)
        .then(function(foto){
            res.json(foto);
        }, function(error){
            console.log(error);
            res.sendStatus(500);
        });
    };
    
    return api;
};
