var mongoose = require("mongoose");
var jwt = require("jsonwebtoken");

module.exports = function(app){

    var api = {};
    var model = mongoose.model('Usuario');

    api.autenticar = function(req, res){
        model.findOne({
            login : req.body.login,
            senha : req.body.senha
        })
        .then(function(usuario){
            if(!usuario){
                console.log("Usuario e senha inválidos");
                res.sendStatus(401);
            }else{
                var token = jwt.sign( {login : usuario.login} , app.get("secret"),{
                    expiresIn : 86400 // valor em segundo, aqui temos um total de 24 horas
                });
                
                res.set('x-access-token', token); // adicionando token no cabeçalho de resposta
                res.end(); // enviando a resposta
                console.log("Autenticação adicionada a resposta");
            }
           

        });
    }

    api.verificaToken = function(req, res, next){
        var token = req.headers["x-access-token"]; // busca o token no header da requisição
        console.log("token " + token);
        if(token){
            console.log("Token recebido, decodificando");
            jwt.verify(token, app.get("secret"), function(err, decoded){
                if(err){
                    console.log("token rejeitado");
                    return res.sendStatus(401);
                }
                console.log("token aceito");
                
                // guardou o valor decodificado do token na requisição. No caso, o login do usuário.
                req.usuario = decoded;
                console.log("vai pra controller foto")
                next();  
            });
        }else{
            console.log("Token não enviado");
            return res.sendStatus(401);
        }
    }


    return api;
};