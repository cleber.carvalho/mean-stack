module.exports = function(app){
    api = app.api.auth;
    app.post('/autenticar', api.autenticar);
    app.use("/*", api.verificaToken);
}