angular.module("alurapic")
    .factory("TokenInterceptor", function($q, $window, $location){
        
        var interceptor = {};

        interceptor.request = function(config){
            // config.headers = config.headers || {};

            if($window.sessionStorage.token){
                config.headers['x-access-token'] = $window.sessionStorage.token;
                console.log("Adicionando token no header da requisição para ser enviado para o servidor");
            }
            console.log(config);
            return config;
        },

        interceptor.response = function(response){
            var token = response.headers("x-access-token");
            if(token){
                $window.sessionStorage.token = token;
                console.log("Token armazenado no navegador");
            }
            return response;
        },

        interceptor.responseError = function(rejection){

            if(rejection != null && rejection.status === 401){
                console.log("removendo token da sessão");
                delete $window.sessionStorage.token;
                $location.path("/login");
            }
            return $q.reject(rejection);
        }

        return interceptor;

    });